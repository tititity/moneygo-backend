package dv.kotlin.moneygo.config

import dv.kotlin.moneygo.entity.*
import dv.kotlin.moneygo.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import dv.kotlin.moneygo.repository.TransactionRepository
import dv.kotlin.moneygo.repository.UserRepository
import java.time.LocalDate
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner{

    @Autowired
    lateinit var walletRepository: WalletRepository

    @Autowired
    lateinit var transactionRepository: TransactionRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var wallet1 = walletRepository.save(Wallet("titi's wallet",12345.0,false))
        var tran1 = transactionRepository.save(
                Transaction("buy banana",
                        "for lunch",
                        10.0,
                        "",
                        TransactionType.EXPENSE,
                        TransactionCatagories.FOOD,
                        LocalDate.of(2019,3,1))
        )
        var tran2 = transactionRepository.save(
                Transaction("buy lipstic",
                        "for myself",
                        129.0,
                        "",
                        TransactionType.EXPENSE,
                        TransactionCatagories.COSMETIC,
                        LocalDate.of(2019,3,1))
        )

        wallet1.transactions.add(tran1)
        wallet1.transactions.add(tran2)
        tran1.wallet = wallet1
        tran2.wallet = wallet1

        var wallet2 = walletRepository.save(
                Wallet("jaja's wallet",
                        2300.0)
        )
        var tran3 = transactionRepository.save(
                Transaction("buy cat's food",
                        "for cat",
                        179.0,
                        "",
                        TransactionType.EXPENSE,
                        TransactionCatagories.GIFT,
                        LocalDate.of(2019,3,3))
        )
        var tran4 = transactionRepository.save(
                Transaction("work",
                        "making crape",
                        2000.0,
                        "",
                        TransactionType.INCOME,
                        TransactionCatagories.FOOD,
                        LocalDate.of(2019,3,4))
        )

        wallet2.transactions.add(tran3)
        wallet2.transactions.add(tran4)
        tran3.wallet = wallet2
        tran4.wallet = wallet2

        var wallet3 = walletRepository.save(
                Wallet("my wallet",
                        2500.0,
                        false)
        )

        var user1 = userRepository.save(
                User("titi",
                        "k",
                        "titi@gmail.com",
                        "tt123",
                        "")
        )

        var user2 = userRepository.save(
                User("jaja",
                        "t",
                        "jaja2gmail.com",
                        "jj123",
                        "")
        )

        user1.wallets.add(wallet1)
        user2.wallets.add(wallet2)
        user2.wallets.add(wallet3)

        wallet1.user = user1
        wallet2.user = user2
        wallet3.user = user2




    }

}