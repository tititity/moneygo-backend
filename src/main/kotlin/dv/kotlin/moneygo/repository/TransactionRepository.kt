package dv.kotlin.moneygo.repository

import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.TransactionType
import org.springframework.data.repository.CrudRepository

interface TransactionRepository : CrudRepository<Transaction,Long>{
    fun findByWalletId(id: Long): List<Transaction>
    fun findByTransactionType(type: TransactionType): List<Transaction>

}
