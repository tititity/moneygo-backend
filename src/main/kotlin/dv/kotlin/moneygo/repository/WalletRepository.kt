package dv.kotlin.moneygo.repository

import dv.kotlin.moneygo.entity.Wallet
import org.springframework.data.repository.CrudRepository

interface WalletRepository : CrudRepository<Wallet,Long> {
    fun findByUserId(userId: Long): List<Wallet>
    fun findByIsDeletedIsFalse(): List<Wallet>
}