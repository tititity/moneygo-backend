package dv.kotlin.moneygo.repository

import dv.kotlin.moneygo.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User,Long>{

}
