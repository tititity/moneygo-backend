package dv.kotlin.moneygo.service

import dv.kotlin.moneygo.entity.User

interface UserService {

    fun getUsers(): List<User>
    fun save(user: User): User
    fun update(id:Long, user:User) : User
}
