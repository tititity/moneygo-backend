package dv.kotlin.moneygo.service

import dv.kotlin.moneygo.dao.UserDao
import dv.kotlin.moneygo.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class UserServiceImpl: UserService {
    @Autowired
    lateinit var userDao: UserDao
    override fun getUsers(): List<User> {
        return userDao.getUsers()
    }

    @Transactional
    override fun save(user: User): User {
        return userDao.save(user)
    }

    override fun update(id:Long, user: User): User {
        return userDao.update(id, user)
    }

}