package dv.kotlin.moneygo.service

import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.Wallet
import dv.kotlin.moneygo.entity.dto.WalletDto

interface WalletService {
    fun getWallets() : List<Wallet>
    fun save(wallet: Wallet): Wallet
    fun save(userId: Long, wallet: Wallet): Wallet
    fun getWalletById(id: Long): Wallet?
    fun getWalletByUserId(userId: Long): List<Wallet>
    fun update(id:Long, wallet: Wallet): Wallet
    fun remove(id: Long): Wallet?
    fun getWalletByIsDeleted():List<Wallet>
}