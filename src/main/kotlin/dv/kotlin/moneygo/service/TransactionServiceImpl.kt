package dv.kotlin.moneygo.service

import dv.kotlin.moneygo.dao.TransactionDao
import dv.kotlin.moneygo.dao.WalletDao
import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.TransactionType
import dv.kotlin.moneygo.entity.Wallet
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class TransactionServiceImpl: TransactionService {
    @Autowired
    lateinit var transactionDao: TransactionDao
    @Autowired
    lateinit var walletDao: WalletDao

    @Transactional
    override fun save(walletId: Long, transaction: Transaction): Transaction {
        val wallet = walletDao.findById(walletId)
        val transaction = transactionDao.save(transaction)
        wallet?.transactions?.add(transaction)
        walletDao.updateBalance(walletId,transaction)
        return transaction
    }

    override fun findByWalletId(id: Long): List<Transaction> {
        return transactionDao.findByWalletId(id)
    }

    override fun findByTransactionType(type: TransactionType): List<Transaction> {
        return transactionDao.findByTransactionType(type)
    }

    override fun findById(id: Long): Transaction? {
        return transactionDao.findById(id)
    }
}