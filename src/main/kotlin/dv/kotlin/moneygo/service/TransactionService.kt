package dv.kotlin.moneygo.service

import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.TransactionType
import dv.kotlin.moneygo.entity.Wallet

interface TransactionService {
    fun save(walletId:Long, transaction: Transaction): Transaction
    fun findByWalletId(id: Long): List<Transaction>
    fun findByTransactionType(type: TransactionType): List<Transaction>
    fun findById(id: Long): Transaction?
}