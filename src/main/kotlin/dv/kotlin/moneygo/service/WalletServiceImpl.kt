package dv.kotlin.moneygo.service

import dv.kotlin.moneygo.dao.UserDao
import dv.kotlin.moneygo.dao.WalletDao
import dv.kotlin.moneygo.entity.Wallet
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class WalletServiceImpl : WalletService {
    @Autowired
    lateinit var walletDao: WalletDao

    @Autowired
    lateinit var userDao: UserDao
    override fun getWallets(): List<Wallet> {
        return walletDao.getWallets()
    }

    @Transactional
    override fun save(wallet: Wallet): Wallet {
        val user = wallet.user?.let { userDao.save(it) }
        val wallet = walletDao.save(wallet)
        user?.wallets?.add(wallet)
        return wallet
    }

    @Transactional
    override fun save(userId: Long, wallet: Wallet): Wallet {
        val user = userDao.findById(userId)
        val wallet = walletDao.save(wallet)
        wallet.user = user
        user?.wallets?.add(wallet)
        return wallet
    }

    override fun getWalletById(id: Long): Wallet? {
        return walletDao.findById(id)
    }

    override fun getWalletByUserId(userId: Long): List<Wallet> {
        return walletDao.findByUserId(userId)
    }


    override fun update(id:Long, wallet: Wallet): Wallet {
        return walletDao.update(id,wallet)
    }

    @Transactional
    override fun remove(id: Long): Wallet? {
        val wallet = walletDao.findById(id)
        wallet?.isDeleted = true
        return wallet
    }

    override fun getWalletByIsDeleted(): List<Wallet> {
        return walletDao.findByIsDeleted()
    }


}