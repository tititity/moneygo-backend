package dv.kotlin.moneygo.dao

import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.TransactionType

interface TransactionDao {
    fun save(transaction: Transaction): Transaction
    fun findByWalletId(id: Long): List<Transaction>
    fun findByTransactionType(type: TransactionType): List<Transaction>
    fun findById(id: Long): Transaction?

}
