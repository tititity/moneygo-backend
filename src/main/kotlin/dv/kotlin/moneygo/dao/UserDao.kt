package dv.kotlin.moneygo.dao

import dv.kotlin.moneygo.entity.User

interface UserDao {
    fun getUsers(): List<User>
    fun findById(userId: Long): User?
    fun save(user: User): User
    fun update(id:Long, user: User) : User

}
