package dv.kotlin.moneygo.dao

import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.TransactionType
import dv.kotlin.moneygo.repository.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class TransactionDaoImpl: TransactionDao {

    @Autowired
    lateinit var transactionRepository: TransactionRepository

    override fun save(transaction: Transaction): Transaction {
        return transactionRepository.save(transaction)
    }

    override fun findByWalletId(id: Long): List<Transaction> {
        return transactionRepository.findByWalletId(id)
    }

    override fun findByTransactionType(type: TransactionType): List<Transaction> {
        return transactionRepository.findByTransactionType(type)
    }

    override fun findById(id: Long): Transaction? {
        return transactionRepository.findById(id).orElse(null)
    }
}