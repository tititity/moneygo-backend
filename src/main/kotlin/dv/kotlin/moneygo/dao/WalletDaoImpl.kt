package dv.kotlin.moneygo.dao

import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.TransactionType
import dv.kotlin.moneygo.entity.Wallet
import dv.kotlin.moneygo.entity.dto.TransactionDto
import dv.kotlin.moneygo.repository.WalletRepository
import dv.kotlin.moneygo.util.MapperUtil
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class WalletDaoImpl : WalletDao {

    @Autowired
    lateinit var walletRepository: WalletRepository

    override fun getWallets(): List<Wallet> {
        return walletRepository.findAll().filterIsInstance(Wallet::class.java)
    }

    override fun save(wallet: Wallet): Wallet {
        return walletRepository.save(wallet)
    }

    override fun findById(id: Long): Wallet? {
        return walletRepository.findById(id).orElse(null)
    }

    override fun updateBalance(walletId: Long, transaction: Transaction) :Wallet {
        val wallet = walletRepository.findById(walletId).orElse(null)
        val price = transaction.price
        wallet?.let {
            var balance = it.balance!!
            if (transaction.transactionType === TransactionType.INCOME) {
                balance += price!!
                it.balance = balance
            } else {
                balance -= price!!
                it.balance = balance
            }
        }
        return wallet
    }

    override fun findByUserId(userId: Long): List<Wallet> {
        return walletRepository.findByUserId(userId)
    }

    override fun update(id:Long, wallet: Wallet): Wallet {
        val old = walletRepository.findById(id).orElse(null)
        wallet.id = old.id
        if(wallet.name == null){
            wallet.name = old.name
        }
        if(wallet.balance == null){
            wallet.balance = old.balance
        }

        wallet.transactions = old.transactions
        wallet.user = old.user
        val output = MapperUtil.INSTANCE.mapWalletDto(walletRepository.save(wallet))
        val outputDto = MapperUtil.INSTANCE.mapWalletDto(output!!)
        return outputDto
    }

    override fun findByIsDeleted(): List<Wallet> {
        return walletRepository.findByIsDeletedIsFalse()
    }
}