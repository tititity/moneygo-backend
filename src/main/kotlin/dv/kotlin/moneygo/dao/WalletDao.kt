package dv.kotlin.moneygo.dao

import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.Wallet

interface WalletDao {
    fun getWallets() : List<Wallet>
    fun save(wallet: Wallet): Wallet
    fun findById(id:Long): Wallet?
    fun updateBalance(walletId: Long, transaction: Transaction): Wallet
    fun findByUserId(userId: Long): List<Wallet>
    fun update(id:Long, wallet: Wallet): Wallet
    fun findByIsDeleted(): List<Wallet>
}