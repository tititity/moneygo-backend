package dv.kotlin.moneygo.dao

import dv.kotlin.moneygo.entity.User
import dv.kotlin.moneygo.repository.UserRepository
import dv.kotlin.moneygo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class UserDaoImpl: UserDao {
    @Autowired
    lateinit var userRepository: UserRepository
    override fun getUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }

    override fun findById(userId: Long): User? {
        return userRepository.findById(userId).orElse(null)
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun update(id:Long, user: User): User {
        val oldData = userRepository.findById(id).orElse(null)
        user.id = oldData.id
        if(user.firstName === null){
            user.firstName = oldData.firstName
        }
        if(user.lastName === null){
            user.lastName = oldData.lastName
        }
        if(user.email === null){
            user.email = oldData.email
        }
        if(user.password === null){
            user.password = oldData.password
        }
        if(user.imageUrl === null){
            user.imageUrl = oldData.imageUrl
        }

        return userRepository.save(user)

    }

}