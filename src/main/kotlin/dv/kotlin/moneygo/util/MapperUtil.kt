package dv.kotlin.moneygo.util

import dv.kotlin.moneygo.entity.Transaction
import dv.kotlin.moneygo.entity.User
import dv.kotlin.moneygo.entity.Wallet
import dv.kotlin.moneygo.entity.dto.TransactionDto
import dv.kotlin.moneygo.entity.dto.UserDto
import dv.kotlin.moneygo.entity.dto.WalletDto
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source="transactions", target = "trans"))
    fun mapWalletDto(wallet: Wallet?): WalletDto?
    fun mapWalletDto(wallets: List<Wallet>): List<WalletDto>

    @InheritInverseConfiguration
    fun mapWalletDto(walletDto: WalletDto) : Wallet

    @Mappings(
            Mapping(source = "transactionType", target = "type"),
            Mapping(source = "transactionCatagories", target = "catagories")
    )
    fun mapTransactionDto(trans: Transaction?) : TransactionDto?
    fun mapTransactionDto(trans: List<Transaction>) : List<TransactionDto>

    @InheritInverseConfiguration
    fun mapTransactionDto(transDto: TransactionDto):Transaction

    fun mapUserDto(user: User) : UserDto
    fun mapUserDto(users: List<User>) : List<UserDto>

    @InheritInverseConfiguration
    fun mapUserDto(userDto: UserDto) : User

}