package dv.kotlin.moneygo.entity

import javax.persistence.*

@Entity
data class Wallet(var name:String? = "My wallet",
                  var balance:Double? = 0.0,
                  var isDeleted: Boolean? = false) {

    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany(mappedBy = "wallet")
    var transactions = mutableListOf<Transaction>()
    @ManyToOne
    var user:User? = null

}