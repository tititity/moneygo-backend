package dv.kotlin.moneygo.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Transaction(var name:String? = null,
                       var description:String? = null,
                       var price:Double? = null,
                       var imageUrl:String? = null,
                       var transactionType: TransactionType? = null,
                       var transactionCatagories: TransactionCatagories? = null,
                       var date: LocalDate? = null){

    @Id
    @GeneratedValue
    var id:Long? = null

    @ManyToOne
    var wallet: Wallet? = null
}