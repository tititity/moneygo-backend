package dv.kotlin.moneygo.entity.dto

import dv.kotlin.moneygo.entity.TransactionCatagories
import dv.kotlin.moneygo.entity.TransactionType
import java.time.LocalDate

data class TransactionDto(
        var name:String? = null,
        var description:String? = null,
        var price:Double? = 0.0,
        var type: TransactionType? = TransactionType.INCOME,
        var catagories: TransactionCatagories? = TransactionCatagories.FOOD,
        var date: LocalDate? = LocalDate.now(),
        var id: Long? = null
) {
}