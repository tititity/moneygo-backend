package dv.kotlin.moneygo.entity.dto

data class WalletDto(
        var name:String? = null,
        var balance:Double? = null,
        var trans: List<TransactionDto>? = emptyList(),
        var isDeleted: Boolean? = false,
        var id: Long? = null
) {
}