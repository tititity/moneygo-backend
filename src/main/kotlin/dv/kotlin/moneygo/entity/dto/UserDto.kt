package dv.kotlin.moneygo.entity.dto

data class UserDto(var firstName: String? = null,
                   var lastName: String? = null,
                   var email: String? = null,
                   var password: String? = null,
                   var imageUrl: String? = null,
                   var id: Long? = null) {
}