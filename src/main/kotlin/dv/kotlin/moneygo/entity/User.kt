package dv.kotlin.moneygo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class User(var firstName:String? = null,
                var lastName:String? = null,
                var email:String? = null,
                var password:String? = null,
                var imageUrl:String? = null) {
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany(mappedBy = "user")
    var wallets = mutableListOf<Wallet>()
}