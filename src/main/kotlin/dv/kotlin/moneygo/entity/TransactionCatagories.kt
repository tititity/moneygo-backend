package dv.kotlin.moneygo.entity

enum class TransactionCatagories {
    FOOD, TRAVEL, COSMETIC, TRANSPORTATION, STATIONARY, CLOTHES, ELECTRONICS, GIFT
}
