package dv.kotlin.moneygo.entity

enum class TransactionType {
    INCOME, EXPENSE

}
