package dv.kotlin.moneygo.controller

import dv.kotlin.moneygo.entity.dto.WalletDto
import dv.kotlin.moneygo.service.WalletService
import dv.kotlin.moneygo.util.MapperUtil
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class WalletController {

    @Autowired
    lateinit var walletService: WalletService

    @GetMapping("/walletAll")
    fun getWallet() : ResponseEntity<Any> {
        val wallets = walletService.getWallets()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapWalletDto(wallets))
    }

    @PostMapping("/wallet")
    fun addWallet(@RequestBody walletDto: WalletDto) : ResponseEntity<Any> {
        val output =walletService.save(MapperUtil.INSTANCE.mapWalletDto(walletDto))
        val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/addWallet/user/{userId}")
    fun addWallet(@RequestBody walletDto: WalletDto, @PathVariable("userId") userId:Long): ResponseEntity<Any>{
        val output = walletService.save(userId, MapperUtil.INSTANCE.mapWalletDto(walletDto))
        val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/wallet/{id}")
    fun getWalletById(@PathVariable("id") id:Long) :ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapWalletDto(walletService.getWalletById(id))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/wallet/user/{userId}")
    fun getWalletByUserId(@PathVariable("userId") userId: Long):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapWalletDto(walletService.getWalletByUserId(userId))
        output?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PutMapping("/updateWallet/{id}")
    fun updateWallet(@RequestBody walletDto: WalletDto, @PathVariable("id") id:Long) : ResponseEntity<Any>{
        val wallet = MapperUtil.INSTANCE.mapWalletDto(walletDto)
        return ResponseEntity.ok(walletService.update(id,wallet))
    }

    @DeleteMapping("/wallet/{id}")
    fun deleteWallet(@PathVariable("id") id:Long) : ResponseEntity<Any>{
        val wallet = walletService.remove(id)
        val outputWallet = MapperUtil.INSTANCE.mapWalletDto(wallet)
        outputWallet?.let{ return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the wallet is not found")
    }

    @GetMapping("/wallets")
    fun getWalletByIsDeleted():ResponseEntity<Any>{
        val wallets = walletService.getWalletByIsDeleted()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapWalletDto(wallets))
    }

}