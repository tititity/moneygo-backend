package dv.kotlin.moneygo.controller

import dv.kotlin.moneygo.entity.dto.UserDto
import dv.kotlin.moneygo.service.UserService
import dv.kotlin.moneygo.util.MapperUtil
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserController {

    @Autowired
    lateinit var userService: UserService
    @GetMapping("/user")
    fun getUsers(): ResponseEntity<Any> {
        val users = userService.getUsers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(users))
    }

    @PostMapping("/user")
    fun addUser(@RequestBody userDto: UserDto): ResponseEntity<Any>{
        val output = userService.save(MapperUtil.INSTANCE.mapUserDto(userDto))
        val outputDto = MapperUtil.INSTANCE.mapUserDto(output)
        outputDto?.let{ return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PutMapping("/updateUser/{id}")
    fun updateUser(@RequestBody user: UserDto, @PathVariable("id") id:Long): ResponseEntity<Any>{
        val user = MapperUtil.INSTANCE.mapUserDto(user)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(userService.update(id,user)))
    }

}