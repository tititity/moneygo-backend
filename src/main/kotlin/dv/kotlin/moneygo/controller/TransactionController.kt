package dv.kotlin.moneygo.controller

import dv.kotlin.moneygo.entity.TransactionType
import dv.kotlin.moneygo.entity.dto.TransactionDto
import dv.kotlin.moneygo.service.TransactionService
import dv.kotlin.moneygo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class TransactionController {
    @Autowired
    lateinit var transactionService: TransactionService

    @PostMapping("/transaction/wallet/{walletId}")
    fun addTransaction(@RequestBody transactionDto: TransactionDto,
                       @PathVariable("walletId") walletId:Long) : ResponseEntity<Any>{
        val output = transactionService.save(walletId,
                MapperUtil.INSTANCE.mapTransactionDto(transactionDto))
        val outputDto = MapperUtil.INSTANCE.mapTransactionDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/transaction/wallet/{walletId}")
    fun getTransactionByWalletId(@PathVariable("walletId") walletId:Long) : ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapTransactionDto(transactionService.findByWalletId(walletId))
        output?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

    }

    @GetMapping("/transaction/type")
    fun getTransactionByType(@RequestParam("type") type: String):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapTransactionDto(transactionService.findByTransactionType(TransactionType.valueOf(type.toUpperCase())))
        output?.let {return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/transaction/{id}")
    fun getById(@PathVariable("id") id:Long): ResponseEntity<Any>{
        var output = transactionService.findById(id)
        output?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
}